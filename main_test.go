package main

import (
    "bytes"
    "net/http"
    "testing"
    "io/ioutil"
)


func TestGetAllEvents(t *testing.T) {
    url := "http://localhost:6000/score"
    req, err := http.NewRequest("GET", url, nil)
    if err != nil {
        t.Fatal(err)
    }

    client := &http.Client{}
    res, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer res.Body.Close()

    t.Log("response Status:", res.Status)
    if res.StatusCode != 200 {
        t.Errorf("Status code 200 expected but %v found", res.StatusCode)
    }

    body, _ := ioutil.ReadAll(res.Body)
    if body == nil {
        t.Errorf("The result of the Body shouldn't be void: %v", body)
    }
}


func TestCreate(t *testing.T) {
    url := "http://localhost:6000/create"
    var jsonBody = []byte(`{"Equipe": "SKT","Score": 100}`)
    req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonBody))
    req.Header.Set("Content-Type", "application/json")
    if err != nil {
        t.Fatal(err)
    }

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        panic(err)
    }
    defer resp.Body.Close()

    t.Log("response Status:", resp.Status)
    if resp.StatusCode != 201 {
        t.Errorf("Status code 201 expected but %v found", resp.StatusCode)
    }
}
