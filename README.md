###Pour lancer le projet, veuillez recupérer les libraries : 

`` go get ``

S'il y a un problème durant l'instalation des packages alors : 

```
 export GOPATH=~/go
 mkdir ~/go/bin
 export GOBIN=$GOPATH/bin
```

###Ensuite vous pouvez lancer le projet : 

`` go run main.go ``

###Pour ajouter des données ( 2000 requêtes via 10 clients ): 

``ab  -p my-data.json -T application/json  -c 10 -n 2000 -v 2 localhost:6000/create``

###Pour Lancer les tests : 

``go test``

###Pour consommer l'API : 

``cd php ``

``php -S localhost:6001 ``

- Puis se rendre sur les urls correspondant au fichier, examples : 


http://localhost:6001/getAllTeam.php

http://localhost:6001/getOneTeam.php?team=Cloud9

http://localhost:6001/getOneTeam.php?team=TeamSoloMid

http://localhost:6001/deleteOneTeam.php?team=TeamSoloMid
