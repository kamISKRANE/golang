package main

import (
    "encoding/json"
    "fmt"
    "io/ioutil"
    "log"
    "net/http"

    "github.com/gorilla/mux"
)

type event struct {
  Equipe string
  Score  int
}

type allEvents []event

var events = allEvents{
    {
        Equipe: "Cloud9",
        Score:  68,
    },
    {
        Equipe: "TeamSoloMid",
        Score:  47,
    },
}
// @Route "/"
func homeLink(w http.ResponseWriter, r *http.Request) {
    fmt.Fprintf(w, "Le Meilleur gestionnaire de score pour les tournois de League Of Legends !")
}

// @Route "/score"
func getAllEvents(w http.ResponseWriter, r *http.Request) {
    fmt.Println("Nombre d'équipe :  ", len(events))
    w.Header().Set("Content-Type", "application/json")
    //Renvoie un status 200
    w.WriteHeader(http.StatusOK)
    json.NewEncoder(w).Encode(events)
}

// @Route "/score/{Equipe}"
func getOneEvents(w http.ResponseWriter, r *http.Request) {
    //Recupere le nom de l'équipe
    eventID := mux.Vars(r)["Equipe"]

    //parcours de toutes les équipes (events)
    for _, singleEvent := range events {
        if singleEvent.Equipe == eventID {
            // la fonction json.NewEncoder(w) va renvoyer à l'utilisateur le resultat de Encode(singleEvent) encodé
            json.NewEncoder(w).Encode(singleEvent)
            break;
        }
    }
}

// @Route "/events/{Equipe}"
func updateEvent(w http.ResponseWriter, r *http.Request) {
    eventID := mux.Vars(r)["Equipe"]
    var updatedEvent event
    reqBody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Fprintf(w, "Veuillez saisir les données avec le nom de l'équipe ainsi que le score associé pour une mise à jour")
    }
    //Affect
    json.Unmarshal(reqBody, &updatedEvent)

    for i, singleEvent := range events {
        if singleEvent.Equipe == eventID {
            singleEvent.Equipe = updatedEvent.Equipe
            singleEvent.Score = updatedEvent.Score
            events = append(events[:i], singleEvent)
            json.NewEncoder(w).Encode(singleEvent)
        }
    }
}

//Suprime tout les équipe ayant le nom reçu en param   /delete/{Equipe}
func deleteEvent(w http.ResponseWriter, r *http.Request) {
    eventID := mux.Vars(r)["Equipe"]
    var updatedEvent event
    fmt.Println("Nom d'équipe a suprime", eventID)
    reqBody, err := ioutil.ReadAll(r.Body)
    if err != nil {
        fmt.Fprintf(w, "Veuillez saisir les données avec le nom de l'équipe ainsi que le score associé pour une mise à jour")
    }
    //Affecte les données envoyées dans la body de la requête dans la variable updatedEvent
    json.Unmarshal(reqBody, &updatedEvent)
    var tmpEvent = allEvents{}
    //parcours tout les équipes
    for _, singleEvent := range events {

        if singleEvent.Equipe != eventID {
        tmpEvent  = append(tmpEvent , singleEvent)
        }
    }
    //Affiche en sortie le nouveau tableau d'équipe
    fmt.Println("Nouveau tableau d'équipe :  ", tmpEvent)
    //Affecte les nouvelles équipes
    events = tmpEvent
}


func generateHandler(queue chan event) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
	    var newTeam event
        reqBody, err := ioutil.ReadAll(r.Body)
        if err != nil {
            fmt.Fprintf(w, "Veuillez saisir les données avec le nom de l'équipe ainsi que le score associé pour une création")
        }

        //Affecte les données envoyées dans la body de la requêtte dans la variable newTeam
        json.Unmarshal(reqBody, &newTeam)

        go addEquipToQueue(newTeam, queue)
        fmt.Println("L'équipe à été ajouté")
        //Status returned 201
        w.WriteHeader(http.StatusCreated)
	}
}

func addEquipToQueue(newTeam event, queue chan event){
    queue <- newTeam

}

func create(queue chan event) {
    //Boucle infinie qui attend qu'une nouvelle équipe rejoigne la queue pour ensuite l'ajouter dans notre collection events
	for {
		newTeam := <-queue
		events = append(events, newTeam)
		fmt.Println(newTeam)
	}
}

func main() {

    //Creation du Channel + routine sur le call de la methode create()
    queue := make(chan event)
    go create(queue)

    router := mux.NewRouter().StrictSlash(true)
    router.HandleFunc("/", homeLink)
    router.HandleFunc("/create", generateHandler(queue)).Methods("POST")
    router.HandleFunc("/score", getAllEvents).Methods("GET")
    router.HandleFunc("/score/{Equipe}", getOneEvents).Methods("GET")
    router.HandleFunc("/events/{Equipe}", updateEvent).Methods("POST")
    router.HandleFunc("/delete/{Equipe}", deleteEvent).Methods("POST")

    log.Fatal(http.ListenAndServe(":6000", router))
}


