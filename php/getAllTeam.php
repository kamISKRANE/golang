<?php

$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "localhost:6000/score",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);

//Param True de json_decode(), pour recup un Tableau
$data = json_decode($response, true);

if(isset($data) && !empty($data)) {
    foreach ($data as $key => $value) {
        if (isset($value['Equipe']) && isset($value['Score'])) {
            echo 'La Team : <b>' . $value['Equipe'] . '</b> à un score de : <b>' . $value['Score'] . '</b></br>';
        }
    }
}else{
    echo '<b> Il n\'y a pas de Team enregistrées<b>';
}
