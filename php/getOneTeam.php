<?php

$team = $_GET['team'];
$curl = curl_init();

curl_setopt_array($curl, array(
    CURLOPT_URL => "localhost:6000/score/".$team,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
));

$response = curl_exec($curl);

curl_close($curl);

//Param True de json_decode(), pour recup un Tableau
$data = json_decode($response, true);

if(isset($data) && !empty($data)) {
    echo 'La Team : <b>' . $data['Equipe'] . '</b> à un score de : <b>' . $data['Score'] . '</b>';
}else{
    echo 'La Team <b> '.htmlspecialchars($team).'</b> n\'existe pas';
}


